from ignite.metrics import Metric
from ignite.exceptions import NotComputableError

class LogRawResults(Metric):

    def __init__(self, output_transform=lambda x: x, device="cpu"):
        self.outputs = []
        self.trues = []
        super(LogRawResults, self).__init__(output_transform=output_transform, device=device)

    def reset(self):
        self.outputs = []        
        self.trues = []
        super(LogRawResults, self).reset()

    def update(self, output):
        out, y = output[0].detach().cpu().numpy(), output[1].detach().cpu().numpy()
        
        self.outputs.append(out)
        self.trues.append(y)        

    def compute(self):
        outputs = np.concatenate(self.outputs, 0)
        trues = np.concatenate(self.trues, 0)
        
        return {
            'outputs': outputs,
            'trues': trues
        }
        

trainer = create_supervised_trainer(model, optimizer, criterion, device)

metrics = {
    "accuracy": Accuracy(),
    "loss": Loss(criterion),
    'logs': LogRawResults()
}
train_evaluator = create_supervised_evaluator(model, metrics=metrics, device=device)
val_evaluator = create_supervised_evaluator(model, metrics=metrics, device=device)

@trainer.on(Events.ITERATION_COMPLETED(every=1))
def log_training_loss(engine):
    print(f"Epoch[{engine.state.epoch}], Iter[{engine.state.iteration}] Loss: {engine.state.output:.2f}")
    

@trainer.on(Events.EPOCH_COMPLETED)
def log_training_results(trainer):
    train_evaluator.run(tiny_loader)
    metrics = train_evaluator.state.metrics
    print(f"Training Results - Epoch[{trainer.state.epoch}] Avg accuracy: {metrics['accuracy']:.2f} Avg loss: {metrics['loss']:.2f}")
    df = pd.DataFrame(metrics['logs']['outputs'])
    print('Adding trues')
    df['true'] = metrics['logs']['trues']
    df.to_csv(f'train_logs_{trainer.state.epoch:04}.csv', index=False)    


@trainer.on(Events.EPOCH_COMPLETED)
def log_validation_results(trainer):
    val_evaluator.run(tiny_loader)
    metrics = val_evaluator.state.metrics
    print(f"Validation Results - Epoch[{trainer.state.epoch}] Avg accuracy: {metrics['accuracy']:.2f} Avg loss: {metrics['loss']:.2f}")
    df = pd.DataFrame(metrics['logs']['outputs'])    
    df['true'] = metrics['logs']['trues']
    df.to_csv(f'val_logs_{trainer.state.epoch:04}.csv', index=False)    

trainer.run(tiny_loader, max_epochs=3)